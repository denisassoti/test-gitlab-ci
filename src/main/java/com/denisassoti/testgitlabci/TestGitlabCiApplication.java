package com.denisassoti.testgitlabci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestGitlabCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestGitlabCiApplication.class, args);
    }

}
